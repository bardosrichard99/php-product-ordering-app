# PHP Product ordering app - Guide

### Intro
I recommend using version control system (GIT) during development to avoid code loss and start building a portfolio so you will be able to show something when you apply for your first developer job.
> **Note:**  [GIT dokumentation](https://docs.github.com/en/get-started/quickstart/create-a-repo)

### About MarkDown files
Markdown is a lightweight markup language that you can use to add formatting elements to plaintext text documents. Created by John Gruber in 2004, Markdown is now one of the world's most popular markup languages.
> **Note:**  [Source](https://www.markdownguide.org/getting-started/)

#### How to read Markdown file:
- If you see it in github, it automatically shows the preview of the file.  
- If you get it as an .md file, you can open it in VSC and click on "Open Preview to the Side" in the top right corner.
## I. Create files

We need 5 files for this project:
- **connect.php** - To connect the applicaiton with our database
- **login.php** - This will contain the login form to reach the **Dashboard**
- **logout.php** - This will close the session (log the user out)
- **order.php** - This will contain the order form which sends the order details to the database and register the new user
- **dashboard.php** - This page shows the dashboard which contains the users table, the orders table and editing options if the user is admin

## II. Create databases

For this application we need 2 tables
### Users table
Field name|Type
-|-
userid|INT 
username|VARCHAR (100)
password|VARCHAR (100)
role|VARCHAR (100)

### Orders table
Field name|Type
-|-
orderid|INT 
product_name|VARCHAR (100)
amount|INT
status|VARCHAR (100)
userid|INT

<img src="II_table_connections.png" style="border: black solid 3px" width="400"/>

### Database and User creation

- [ ] Load in phpMyAdmin ([Link](http://localhost/phpmyadmin))
- [ ] On the left panel, choose "New"
- [ ] Type in a name and click "Create"
- [ ] On the top menu, choose "Privileges"
- [ ] Click "New user account"
- [ ] Add User name and Password (Make sure "Grant all privileges on database ..." is checked)

> **Note:** Save the user name and password, because we will need it in **connect.php**

### Fill up database with tables and elements

- [ ] On the top menu, choose "SQL" (Make sure your database is selected on the left panel)
- [ ] Create **Users** table
    ``` sql
    CREATE table Users (
        `userid` INT NOT NULL AUTO_INCREMENT,
        `username` VARCHAR (100) COLLATE utf8_general_ci NOT NULL,
        `password` VARCHAR (100) COLLATE utf8_general_ci NOT NULL,
        `role` VARCHAR (100) COLLATE utf8_general_ci NOT NULL,
        PRIMARY KEY (`userid`),
        UNIQUE (`username`)
    )
    ```
- [ ] Create **Orders** table
    ``` sql
    CREATE table Orders (
        `orderid` INT NOT NULL AUTO_INCREMENT,
        `product_name` VARCHAR (100) COLLATE utf8_general_ci NOT NULL,
        `amount` INT NOT NULL,
        `status` VARCHAR (100) COLLATE utf8_general_ci NOT NULL,
        `userid` INT NOT NULL,
        PRIMARY KEY (`orderid`),
        FOREIGN KEY (`userid`) REFERENCES Users(`userid`)
    )
    ```
- [ ] Add **admin user** to **Users** table
    > **Note:** We need to add Admin user because the only way to register is to send a order and it adds "user" role to the new user automatically
    ``` sql
    INSERT INTO Users (
        username, password, role
    ) VALUES (
        'admin','d033e22ae348aeb5660fc2140aec35850c4da997','admin'
    )
    ```
    > **Note:** The password is the hashed version of the 'admin' string (detailed informations in IX section)

## III. Setup connections

**Purpose:** This file contains the the database credentials, the connection with the database and the error message if the connection fails

### Structure & Steps

- Database details  
    * Create 4 variables for the 4 information we need about the database  
    (server name, user name, password, database name)
        ```php
        $servername = "localhost"; // You can use "127.0.0.1" as well
        $username = "admin"; // Your username
        $password = "admin"; // Your password
        $db_name = "phporderapp"; // Your database name
        ```
- Global variable to store the connection informations
    * Make a global variable
        ```php
        global $conn;
        ```
        > **Note**: global variables are reachable across files (detailed informations in IX section)
- Connection
    * Make a new connection with the Database details and store in the global variable
        ```php
        $conn = new mysqli($servername, $username, $password, $db_name);
        ```
- Check connection
    * Write an IF statement where you check the **connect_error** property of the global variable and display an error message (You can use [die()](https://www.php.net/manual/en/function.die.php) as well)
        ```php
        if ($conn->connect_error) {
            die("Connection error: " . $conn->connect_error);
        }
        ```

## IV. Create Login file & Redirection

### How it looks like

<img src="IV_login.png" style="border: black solid 3px" width="400"/>

### Database usage

1. **GET** request which checks whether the username and password from the form is existing in the database

### Logic

- When the user clicks the Login button we make a **POST** request to the same **login.php** file.  
- We get the username and password from the request and store it in variables  
- **IF** the username or password is empty, we show an error message
- **ELSE**
    * We hash the password
    * We make a **GET** request to the database with the username and password
    * **IF** the response is empty, we show an error message
    * **ELSE**
        + We create session variables
        + Show success message
        + Redirect to **dashboard.php**

### Structure & Steps

- Create header
    * Start the session
        ```php
        session_start();
        ```
    * Create a header with proper encoding
        ```php
        header("Content-type: text/html; charset=utf-8");
        ```
- Connect server
    * Import connect.php (we only need once)
        ```php
        require_once "connect.php";
        ```
    * Declare the global variable
        ```php
        global $conn;
        ```
- Redirection if we logged in
    * Make an IF statement which checks the session and redirect to dashboard.php if user is logged in
        ```php
        if (isset($_SESSION['loggedin'])) {
            header('Location: dashboard.php');
        }
        ```
- Setup HTML document
    * Create the wireframe of the HTML document
        ```html
        <!DOCTYPE HTML>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Login</title>
            </head>
            <body>
                <!-- Form -->
            </body>
        </html>
        <!-- PHP functions -->
        ```
        > **Note**: We have to put the PHP code in the end because we change values (error message) in the html and we can only do if we create it before the function
- Form setup
    * Create a form with a title, 2 input fields with proper labels, submit button and an empty subtitle with red color
        ```html
        <h1>Login</h1>
        <form>
            <h4 id="error" style="color: red"></h4>
            <label for="username">User:</label>
            <input type="text" name="username" id="username">
            <br><br>
            <label for="password">Password:</label>
            <input type="password" name="password" id="password">
            <br><br>
            <button type="submit" name="submit">Login</button>
        </form>
        ```
    * Add **POST** request parameters to form tag
        ```html
        <!-- ... -->
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
        <!-- ... -->
        ```
- Create the Login logic
    * Create an if statement which checks if the submition is made
        ```php
        if (isset($_POST["submit"])) {
            // Rest of the code
        }
        ```
    * Get the datas from the request, convert them to string and store them in variables
        ```php
        $username = $conn->real_escape_string($_POST["username"]);
        $password = $conn->real_escape_string($_POST["password"]);
        ```
        > **Note**: [real_escape_string](https://www.php.net/manual/en/mysqli.real-escape-string.php)
    * Check **IF** the username OR password is empty and display a message
        ```php
        if (empty($username) || empty($password)) {
            ?>
            <script>
                document.querySelector('#error').innerText = "Please fill in both fields!";
            </script>
            <?php
        } else {
            // Rest of the code
        }
        ```
        > **Note**: This statement shows an error message by setting the subtitle's value, we added before  
        > **Note**: We can put this code into a separate function, but we only use it 3 times in this file
    * Hash the password
        ```php
        $password = sha1($password);
        ```
        > **Note**: We have to do it here because if we hash before and we check the emptiness it will cause error!  
        Hash of "" = "da39a3ee5e6b4b0d3255bfef95601890afd80709"
    * Make the **GET** request to database where the query contains the username and password
        ```php
        $sql = "SELECT Count(*) FROM users WHERE username = '{$username}' AND password = '{$password}'";
        $result = $conn->query($sql);        
        ```
    * Check **IF** the request succeeded, **ELSE** show an error message
        ```php
        if ($result->num_rows > 0) {
            // Rest of the code
        } else {
            ?>
            <script>
                document.querySelector("#error").innerText = "Unsuccessful DataBase action";
            </script>
            <?php
        }
        ```
    * Convert the response and check if the query had result or not (if not, show error message)
        ```php
        $row_result = $result->fetch_assoc();
        if ($row_result['Count(*)'] > 0) {
            // Rest of the code
        } else {
            ?>
            <script>
                document.querySelector("#error").innerText = "Wrong Username / Password!";
            </script>
            <?php
        }
        ```
        > **Note**: [fetch_assoc](https://www.php.net/manual/en/mysqli-result.fetch-assoc.php)
    * Create session variables, show success message and redirect to **dashboard.php**
        ```php
        $_SESSION["loggedin"] = true;
        $_SESSION["username"] = $username;
        echo "Sikeres bejelentkezés";
        header('Location: dashboard.php');
        exit();
        ```

## V. Create Logout file & Logout function

**Purpose:** This file contains the logout function, which ends the session and redirects to **login.php**

### Structure & Steps

- Session start
    * Start the session
        ```php
        session_start();
        ```
- Session ending **IF** session is existing
    * Create an **IF** statement, **IF** session exists distroy it.
        ```php
        if (isset($_SESSION['loggedin'])){
            session_unset();
            session_destroy();
        }
        ```
        > **Note**: [session_unset](https://www.php.net/manual/en/function.session-unset.php) & [session_destroy](https://www.php.net/manual/en/function.session-destroy.php)
- Redirect to **login.php**
    * Create a redirection to **login.php**
        ```php
        header('Location: login.php');
        ```

## VI. Create Order page

### How it looks like

<img src="VI_order.png" style="border: black solid 3px" width="400"/>

### Database usage

1. **GET** request which checks whether the username from the form is existing in the database
2. **POST** request which creates a new user in Users table
3. **GET** request which collects the given user's information for the order
4. **POST** request which creates a new order in Orders table

### Logic

- When user checks "Already registered", password field disappears
- When user clicks the Send order button we make a **POST** request to the same **order.php** file.  
- We get all the field informations from the request and store it in variables  
- **IF** the username, product or amount is empty, we show an error message
- **ELSE** 
    * We make a **GET** request to the database with the username
    * **IF** the response is empty, we show an error message
    * **ELSE**
        + We check if the request succeeded
        + **IF** the username not exists
            + We check **IF** the the "Already registered" checked -> Error message
            + We check **IF** the password field is empty -> Error message
            + **ELSE** we hash the password, send Registration and Order
        + **IF** the username exists
            + We check **IF** the "Already registered" NOT checked -> Error message
            + **ELSE** we send the Order

### Structure & Steps

- Create header
    * Create a header with proper encoding
        ```php
        header("Content-type: text/html; charset=utf-8");
        ```
- Connect server
    * Import **connect.php** (we only need once)
        ```php
        require_once "connect.php";
        ```
    * Declare the global variable
        ```php
        global $conn;
        ```
- Setup HTML document
    * Create the wireframe of the HTML document
        ```html
        <!-- JavaScript code -->
        <!DOCTYPE HTML>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Order</title>
            </head>
            <body>
                <!-- Form -->
            </body>
        </html>
        <!-- PHP functions -->
        ```
- Form setup
    * Create a form with  
    a title,  
    a checkbox with label,  
    2 input fields with proper labels, 
    a dropdown with 3 elements and label,  
    a number input field with label,   
    submit button,  
    2 empty subtitle one with red, one with green color
        ```html
        <h1>Order</h1>
        <h4 id="error" style="color: red"></h4>
        <h4 id="success" style="color: green"></h4>
        <form>
            <label for="registered">Already registered</label>
            <input type="checkbox" name="registered" id="registered" value="Registered">
            <br><br>
            <label for="username">User:</label>
            <input type="text" name="username" id="username">
            <br><br>
            <label id="pw-label">Password:
                <input type="password" name="password" id="password">
                <br><br>
            </label>
            <label for="product">Product:</label>
            <select name="product" id="product">
                <option value="apple">Apple</option>
                <option value="banana">Banana</option>
                <option value="orange">Orange</option>
            </select>
            <br><br>
            <label for="amount">Amount:</label>
            <input type="number" name="amount" id="amount" value="1">
            <br><br>
            <button type="submit" name="submit">Send order</button>
        </form>
        ```
    * Add **POST** request parameters to form tag
        ```html
        <!-- ... -->
        <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
        <!-- ... -->
        ```
    * Add onClick event handler for the checkbox (we will implement later)
        ```html
        <!-- ... -->
        <input type="checkbox" name="registered" id="registered" onclick="onRegister()" value="Registered">
        <!-- ... -->
        ```
    * Add minimum and maximum limit to the number input field
        ```html
        <!-- ... -->
        <input type="number" name="amount" id="amount" min="1" max="50" value="1">
        <!-- ... -->
        ```
- Create JavaScript logic for "Already registered" checkbox
    * Create JavaScript function which handles with the checkbox onClick event
        ```html
        <script>
            function onRegister() {
                // Rest of the code
            }
        </script>
        ```
    * Create 2 variables and store the checkbox and the password label
        ```JavaScript
        var checkBox = document.getElementById("registered");
        var pwLabel = document.getElementById("pw-label");
        ```
        > **Note**: We choose the label because we want to make both the label and the input field disappear, thats why the label contains the input field
    * Create an **IF** statement which checks the checkbox's value and sets the password's visibility
        ```JavaScript
        if (checkBox.checked == true) {
            pwLabel.style.display = "none";
        } else {
            pwLabel.style.display = "inline";
        }
        ```
        > **Note**: We need to still check the password field's value later, because the POST request still sends it if it is visible, if not
- Create the structure of the PHP functions
    * Create the Form submit, a Register, an Order and a Message function in the bottom
        ```php
        // ...
        if (isset($_POST["submit"])) {
            // This statement will be called if the submit button is clicked
        }
        function Register(){
            // This function will be called from the Submittion and will create a new user
        }
        function Order(){
            // This function will be called from the Submittion and will create a new order
        }
        function Message(){
            // This function will be called from many places and will set the value of the error and success message
        }
        ```
- Create Message function
    * Add the following 2 inputs to the function: **isError**, **message**
        ```php
        function Message($isError, $message) {
        // Rest of the code
        }
        ```
        > **Note**: IsError contains the type of the message ('error' or 'success')
    * Create an **IF** statement which checks whether the message is error or success and set the appropriate subtitle
        ```php
        if ($isError === 'error') {
            ?>
            <script>
                document.querySelector("#error").innerText = "<?php echo $message ?>";
            </script>
            <?php
        } else if ($isError === 'success') {
            ?>
            <script>
                document.querySelector("#success").innerText = "<?php echo $message ?>";
            </script>
            <?php
        }
        ```
- Create Register function
    * Add the following 3 inputs to the function: **global variable**, **username**, **password**
        ```php
        function Register($conn, $username, $password){
        // Rest of the code
        }
        ```
    * Create a **POST** request which adds a new user to the Users table
        ```php
        $sql = "INSERT INTO users (username, password, role) VALUES ('{$username}', '{$password}', 'user')";
        ```
        > **Note**: We automatically set role to 'user'
    * Create an **IF** statement which checks whether the request was successful or not and sets the appropriate **Message**
        ```php
        if ($conn->query($sql) === TRUE) {
            Message('success', "Client registered successfully");
        } else {
            Message('error', "Error: {$sql} - {$conn->error}");
        }
        ```
- Create Order function
    * Add the following 4 inputs to the function: **global variable**, **username**, **product**, **amount**
        ```php
        function Order($conn, $username, $product, $amount){
        // Rest of the code
        }
        ```
    * Create a **GET** request which contains the user's informations and convert it
        ```php
        $sql = "SELECT * FROM users WHERE username = '{$username}'";
        $resultUser = $conn->query($sql);
        $user_datas = $resultUser->fetch_assoc();
        ```
    * Create a **POST** request which adds a new order to the Orders table
        ```php
        $sql = "INSERT INTO orders (product_name, amount, status, userid) VALUES ('{$product}', '{$amount}', 'open', '{$user_datas['userid']}')";
        ```
        > **Note**: We automatically set status to 'open'
    * Create an **IF** statement which checks whether the request was successful or not and sets the appropriate **Message**
        ```php
        if ($conn->query($sql) === TRUE) {
            Message('success', "Client registered successfully & Order sent");
        } else {
            Message('success', "");
            Message('error', "Error: {$sql} - {$conn->error}");
        }
        ```
- Create Submittion function
    * Create an **IF** statement which checks if the submition is made
        ```php
        if (isset($_POST["submit"])) {
            // Rest of the code
        }
        ```
    * Get the datas from the request, convert them to string and store them in variables
        ```php
        $username = $conn->real_escape_string($_POST["username"]);
        $password = $conn->real_escape_string($_POST["password"]);
        $product = $conn->real_escape_string($_POST["product"]);
        $amount = $conn->real_escape_string($_POST["amount"]);
        ```
    * Check **IF** the username, product OR amount is empty and display a message
        ```php
        if (empty($username) || empty($product) || empty($amount)) {
            Message('error', "Please fill in all fields!");
        } else {
            // Rest of the code
        }
        ```
    * Make a **GET** request to database where the query contains the username
        ```php
        $sql = "SELECT Count(*) FROM users WHERE username = '{$username}'";
        $resultCount = $conn->query($sql);       
        ```
    * Check **IF** the request succeeded, **ELSE** show an error message
        ```php
        if ($result->num_rows > 0) {
            // Rest of the code
        } else {
            Message('error', "Unsuccesful database action");
        }
        ```
    * Convert the response and check **IF** the query had result or not
        ```php
        $row_result = $result->fetch_assoc();
        if ($row_result['Count(*)'] <= 0) {
            // IF user NOT exists
                // BUT registered checked
                // BUT registered NOT checked AND password is empty
                // BUT registered NOT checked AND password is NOT empty
        } else {
            // IF user exists
                // BUT registered checked
                // BUT registered NOT checked
        }
        ```
        + Create logic: **IF** user **NOT** exists **BUT** registered checked  
        -> **show error message**
            ```php
            Message('error', "Username not found, please provide a password");
            ```
        + Create logic: **IF** user **NOT** exists **BUT** registered **NOT** checked AND password is empty  
        -> **show error message**
            ```php
            Message('error', "Please provide a password");
            ```
        + Create logic: **IF** user **NOT** exists **BUT** registered **NOT** checked **AND** password is **NOT** empty  
        -> **hash password and call Register and Order functions**
            ```php
            $password = sha1($password);
            Register($conn, $username, $password);
            Order($conn, $username, $product, $amount);
            ```
        + Create logic: **IF** user exists **BUT** registered checked  
        -> **call Order function**
            ```php
            Order($conn, $username, $product, $amount);
            ```
        + Create logic: **IF** user exists **BUT** registered **NOT** checked  
        -> **show error message**
            ```php
            Message('error', "Already existing Username, please check the checkbox");
            ```

## VII. Create Dashboard page - tables

### How it looks like

<img src="VII_dashboard_tables.png" style="border: black solid 3px" width="400"/>

> **Note**: You need to send orders to test the application

### Database usage

1. **GET** request which collects the current user's informations
2. **GET** request to Users table which collects
    - The current user's informations **IF** the user's role is **NOT** 'admin'
    - All users' informations **IF** the user's role is 'admin'
3. **GET** request to Orders table which collects
    - All the orders connected to the current user **IF** the user's role is **NOT** 'admin'
    - All order informations **IF** the user's role is 'admin'

### Logic

- When user logges in, this page become visible and will be redirected here
- It welcomes the user
- It has a log out button
- **IF** the user's role is 'admin'
    * It shows a table with all the users and their informations
    * It shows a table with all the orders and their informations
- **IF** the user's role is 'user'
    * It shows a table with the current user's informations
    * It shows a table with all the orders and their informations connected to the current user

### Structure & Steps

- Create header
    * Start the session
        ```php
        session_start();
        ```
    * Create a header with proper encoding
        ```php
        header("Content-type: text/html; charset=utf-8");
        ```
- Redirection **IF** we logged in
    * Make an **IF** statement which checks the session and redirect to **login.php** **IF** user is **NOT** logged in
        ```php
        if (empty($_SESSION['loggedin'])) {
            header('Location: login.php');
        }
        ```
- Connect server
    * Import **connect.php** (we only need once)
        ```php
        require_once "connect.php";
        ```
    * Declare the global variable
        ```php
        global $conn;
        ```
- Create a variable for errors
    * Create a local variable for error messages
        ```php
        $errormsg;
        ```
        > **Note**: We use a different error messaging mechanism in this file, that is the reason why we need this
- **GET** the current user's informations
    * Create a **GET** request which returns the current user's informations and convert it
        ```php
        $sql = "SELECT * FROM users WHERE username = '{$_SESSION['username']}'";
        $result = $conn->query($sql);
        $currentUserDatas = $result->fetch_assoc();
        ```
- Setup HTML document
    * Create the wireframe of the HTML document
        ```html
        <!-- VIII PHP extention -->
        <!DOCTYPE HTML>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Dashboard</title>
                <style>
                    /* CSS code */
                </style>
            </head>
            <body id="content">
                <!-- Welcome message -->
                <!-- Log out -->
                <!-- Users Table -->
                <!-- Orders Table -->

                <!-- VIII PHP + HTML extention -->
            </body>
        </html>
        <!-- PHP functions -->
        ```
- Welcome message
    * Create a Welcome Message with the current user's username
        ```html
        <h1>Welcome,<?= $_SESSION['username'] ?>!</h1>
        ```
        > **Note**: "```<?= $a ?>```" is the shorthand for "```<?php echo $a ?>```" since 5.4.0
- Log Out functionality
    * Create a section for logging out
        ```html
        <section id="logout-form">
            <form id="logout-action" action="logout.php" method="post">
                <a href="javascript:{}" onclick="document.getElementById('logout-action').submit();">Log out</a>
            </form>
        </section>
        ```
- Users table
    * Create the wireframe of the section
        ```html
        <section id="section-users">
            <h3>User datas</h3>
            <table>
                <!-- Header (html) -->
                <!-- Body (php) -->
            </table>
        </section>
        ```
    * Create the Header of the table
        ```html
        <thead>
            <tr>
                <td>ID</td>
                <td>User</td>
                <td>Password</td>
                <td>Role</td>
            </tr>
        </thead>
        ```
    * Create the Body
        + Create the **GET** request for the table with the logic about the 2 roles  
        -> **IF admin, show all users, IF not admin, show only current user**
            ```php
            if ($currentUserDatas['role'] == 'admin') {
                $sql = 'SELECT * FROM users';
            } else {
                $sql = 'SELECT * FROM users WHERE userid = ' . $currentUserDatas['userid'];
            }
            $result = $conn->query($sql);
            // Rest of the code
            ```
        + Create the table by mapping out the results of the query
            ```php
            while ($row = $result->fetch_assoc()) {
                printf('
                    <tr>
                        <td>%s</td>
                        <td>%s</td>
                        <td>%s</td>
                        <td>%s</td>
                    </tr>
                    ', $row['userid'], $row['username'], $row['password'], $row['role']);
            };
            ```
            > **Note**: [printf](https://www.php.net/manual/en/function.printf.php) (detailed informations in IX section)
- Orders table
    * Create the wireframe of the section
        ```html
        <section id="section-orders">
            <h3>Order list</h3>
            <table>
                <!-- Header (html) -->
                <!-- Body (php) -->
            </table>
        </section>
        ```
    * Create the Header of the table
        ```html
        <thead>
            <tr>
                <td>ID</td>
                <td>Product</td>
                <td>Amount</td>
                <td>Status</td>
                <td>User ID</td>
            </tr>
        </thead>
        ```
    * Create the Body
        + Create the **GET** request for the table with the logic about the 2 roles  
        -> **IF admin, show all orders, IF not admin, show only current user's orders**
            ```php
            if ($currentUserDatas['role'] == 'admin') {
                $sql = 'SELECT * FROM orders';
            } else {
                $sql = 'SELECT * FROM orders WHERE userid = ' . $currentUserDatas['userid'];
            }
            $result = $conn->query($sql);
            // Rest of the code
            ```
        + Create the table by mapping out the results of the query
            ```php
            while ($row = $result->fetch_assoc()) {
                printf('
                <tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                </tr>
                ', $row['orderid'], $row['product_name'], $row['amount'], $row['status'], $row['userid']);
            };
            ```
            > **Note**: [printf](https://www.php.net/manual/en/function.printf.php) (detailed informations in IX section)
## VIII. Extend Dashboard page with editors


### How it looks like

<img src="VIII_dashboard_editors.png" style="border: black solid 3px" width="400"/>

### Database usage

1. **GET** request to Users which collects all usernames for edit role dropdown
2. **GET** request to Orders which collects all order IDs for edit status dropdown
3. **GET** request to Users which collects the picked user's informations
4. **GET** request to Orders which collects the picked order's informations
5. **UPDATE** request to Users which changes the role of the picked user
6. **UPDATE** request to Orders which changes the status of the picked order

### Logic

- The 2 editing section only shows up **IF** the current user is admin
- The 2 editing section works with **POST** request to the same **dashboard.php** file.
- The User role editor changes the role of the picked user (admin -> user, user -> admin)
    * You can't change the admin user's role
    * You can't change the current user's role
- The Order status editor changes the status of the picked order (open -> closed, closed -> open)

### Structure & Steps

- Check the current file structure & Place the new component wireframes
    * Place the PHP sections into **dashboard.php**  
    -> Place the 2 php functions in the top PHP section (role change, status change)  
    -> Place the section visibility PHP function under the HTML section  
    -> Place the error message PHP function under the visibility function
        ```php
        // ...
        $sql = "SELECT * FROM users WHERE username = '{$_SESSION['username']}'";
        $result = $conn->query($sql);
        $currentUserDatas = $result->fetch_assoc();

        // Role change function
        // Status change function

        <!DOCTYPE HTML>
        <html lang="en"> 
        // ... 
        </html>

        // Visibility function
        // Error message
        ```
    * Place the HTML sections into **dashboard.php**  
    -> Place the visibility PHP function DOM part into the end of the HTML section  
    -> Place the error message section after the visibility  
    -> Place the 2 editor sections after the error message
        ```html
        <!-- ... -->
        <!DOCTYPE HTML>
        <html lang="en"> 
            <h1>Welcome ... </h1>
            <section id="logout-form"> ... </section>
            <section id="section-clients"> ... </section>
            <section id="section-orders"> ... </section>
            <!-- Visibility function PHP function --> 
            <!-- Error message section --> 
            <!-- User role change section --> 
            <!-- Order status change section --> 
        </html>
        <!-- ... -->
        ```
- Setup the editor section's visibility
    * Place the ob_start PHP function into the HTML section
        ```php
        // <section id="section-orders"> ... </section>
        ob_start()
        // Rest of the code
        ```
        > **Note**: [ob_start](https://www.php.net/manual/en/function.ob-start.php) function creates an output buffer, so we can set the visibility of the component below this point (detailed informations in IX section)
    * Create a variable and initialize the ob_get_clean function (below HTML section)
        ```php
        // ...
        // </html>
        $ob_result = ob_get_clean();
        // Rest of the code
        ```
        > **Note**: [ob_get_clean](https://www.php.net/manual/en/function.ob-get-clean.php): Gets the buffer content (detailed informations in IX section)
    * Create the visibility statement by echo the $ob_result
        ```php
        if ($currentUserDatas['role'] === "admin") {
            echo $ob_result;
        }
        ```
- Set the error message function
    * Create an **IF** statement which shows the proper message **IF** $errormsg is set
        > **Note**: $errormsg is an array with 2 elements: type("error" or "success") and message(string)
        ```php
        if (isset($errormsg) and $errormsg[0] === 'error') {
            ?>
            <script>
                document.querySelector("#error").innerText = "<?php echo $errormsg[1] ?>";
            </script>
            <?php
        } else if (isset($errormsg) and $errormsg[0] === 'success') {
            ?>
            <script>
                document.querySelector("#success").innerText = "<?php echo $errormsg[1] ?>";
            </script>
            <?php
        }
        ```
- Setup the Error message section in the HTML section
    * Create a section with 2 subtitles, one with red, one with green color
        ```html
        <section>
            <h4 id="error" style="color: red;"></h4>
            <h4 id="success" style="color: green;"></h4>
        </section>
        <!-- Rest of the code -->
        ```
- Setup the User role editor
    * Create the wireframe of the section; we need a title, a dropdown and a button
        ```html
        <section>
            <h3>User role change (by ursername)</h3>
            <form id="rolechange-action">
                <select name="users" id="users">
                    <!-- PHP code -->
                </select>
                <input type="submit" name="rolechange" value="Change Role">
            </form>
        </section>
        ```
    * Set the **POST** request of the form
        ```html
        <form id="rolechange-action" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
        ```
    * Create a **GET** request which gets all usernames and fill up the dropdown
        ```php
        <select name="users" id="users">
            <?php 
                $sql = 'SELECT * FROM users';
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc())
                    printf('<option value="%s">%s</option>', $row['username'], $row['username']);
            ?> 
        </select>
        ```
- Setup the Order status editor
    * Create the wireframe of the section; we need a title, a dropdown and a button
        ```html
        <section>
            <h3>Order status change (by ID)</h3>
            <form id="orderstatuschange-action">
                <select name="orders" id="orders">
                    <!-- PHP code -->
                </select>
                <input type="submit" name="orderstatuschange" value="Change Order Status">
            </form>
        </section>
        ```
    * Set the **POST** request of the form
        ```html
        <form id="orderstatuschange-action" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
        ```
    * Create a **GET** request which gets all order IDs and fill up the dropdown
        ```php
        <select name="users" id="users">
            <?php 
                $sql = 'SELECT * FROM orders';
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc())
                    printf('<option value="%s">%s</option>', $row['orderid'], $row['orderid']);
            ?> 
        </select>
        ```
- Setup the Role changing functionality
    * Create an **IF** statement which checks **IF** the submition is made
        ```php
        if (isset($_POST['rolechange'])) {
            // Rest of the code
        }
        ```
    * Get the picked username from the request, convert it to string and store it in a variable
        ```php
        $pickedUsername = $conn->real_escape_string($_POST["users"]);
        // Rest of the code
        ```
    * Check **IF** the username is empty and display a message
        ```php
        if (empty($pickedUsername)) {
            $errormsg = array("error", "Please choose a username");
        } 
        // Rest of the code
        ```
    * Check **IF** the picked username is "admin"
        ```php
        else if ($pickedUsername == 'admin') {
            $errormsg = array("error", "Can not change the admin's role");
        }
        // Rest of the code
        ```
    * Check **IF** the picked username is equal with the current username
        ```php
        else if ($pickedUsername == $currentUserDatas["username"]) {
            $errormsg = array("error", "Can not change your own role");
        } else {
            // Rest of the code
        }
        ```
    * Make a **GET** request which collects all the information about the picked user and convert it
        ```php
        $sql = "SELECT * FROM users WHERE username = '{$pickedUsername}'";
        $resultUser = $conn->query($sql);
        $user_datas = $resultUser->fetch_assoc();
        // Rest of the code
        ```
    * Create a statement which checks the picked user's role and set a variable with the opposite
        ```php
        $newRole;
        $user_datas['role'] == 'admin' ? $newRole = 'user' : $newRole = 'admin';
        // Rest of the code
        ```
    * Create the **UPDATE** request which update the picked user's role in the database and show the appropriate message
        ```php
        $sql2 = "UPDATE users SET role='{$newRole}' WHERE username='{$pickedUsername}'";
        if ($conn->query($sql2) === TRUE) {
            $errormsg = array("success", "Client '{$pickedUsername}' role successfully changed");
        } else {
            $errormsg = array("error", "Hiba: {$sql2} - {$conn->error}");
        }
        ```
- Setup the Status changing functionality
    * Create an **IF** statement which checks **IF** the submition is made
        ```php
        if (isset($_POST['orderstatuschange'])) {
            // Rest of the code
        }
        ```
    * Get the picked order ID from the request, convert it to string and store it in a variable
        ```php
        $pickedOrderid = $conn->real_escape_string($_POST["orders"]);
        // Rest of the code
        ```
    * Check **IF** the order ID is empty and display a message
        ```php
        if (empty($pickedOrderid)) {
            $errormsg = array("error", "Please choose an order id");
        } else {
            // Rest of the code
        }
        ```
    * Make a **GET** request which collects all the information about the picked order and convert it
        ```php
        $sql = "SELECT * FROM orders WHERE orderid = '{$pickedOrderid}'";
        $resultOrder = $conn->query($sql);
        $order_datas = $resultOrder->fetch_assoc();
        // Rest of the code
        ```
    * Create a statement which checks the picked order's status and set a variable with the opposite
        ```php
        $newStatus;
        $order_datas['status'] == 'open' ? $newStatus = 'closed' : $newStatus = 'open';
        // Rest of the code
        ```
    * Create the **UPDATE** request which update the picked order's status in the database and show the appropriate message
        ```php
        $sql2 = "UPDATE orders SET status='{$newStatus}' WHERE orderid='{$pickedOrderid}'";
        if ($conn->query($sql2) === TRUE) {
            $errormsg = array("success", "Order number '{$pickedOrderid}' status successfully changed");
        } else {
            $errormsg = array("error", "Hiba: {$sql2} - {$conn->error}");
        }
        ```

## IX. Additional knowledge collection

### Password Hash

The [sha1()](https://www.php.net/manual/en/function.sha1.php) function calculates the SHA-1 hash of a string.

```php
$str = "Hello";
echo sha1($str);
// output: f7ff9e8b7bb2e09b70935a5d785e0cc5d9d0abf0
```

> **SHA-1**: SHA-1 or **S**ecure **H**ash **A**lgorithm 1 is a cryptographic algorithm which takes an input and produces a 160-bit (20-byte) hash value. This hash value is known as a message digest. This message digest is usually then rendered as a hexadecimal number which is 40 digits long. It is a U.S. Federal Information Processing Standard and was designed by the United States National Security Agency.  
[Source](https://www.geeksforgeeks.org/sha-1-hash-in-java/)

> **Note**: SHA-1 is now considered insecure since 2005 (as you see the Warning message in php.net)  
> **Alternative**: [crypt()](https://www.php.net/manual/en/function.crypt.php)

### Content Buffering

**Aim**: We use Buffering when we would like to handle contents conditionally

```php
// Create an output buffer 
ob_start(); 

// Conditional content
echo "This will not be shown"; 

// We store all the content between ob_start() and ob_get_clean()
$out = ob_get_clean(); 

// Content after ob_get_clean() will be shown
echo "This will be shown"; 

// We show content conditionally
if (true){
    echo '<br>';
    echo $out;
}
```
```php
// Output
This will be shown
This will not be shown
```

The [ob_start()](https://www.php.net/manual/en/function.ob-start) function creates an output buffer. A callback function can be passed in to do processing on the contents of the buffer before it gets flushed from the buffer. Flags can be used to permit or restrict what the buffer is able to do.

The [ob_get_clean()](https://www.php.net/manual/en/function.ob-get-clean.php) function is an in-built PHP function that is used to **clean** or delete the current output buffer. It’s also used to **get** the output buffering again after cleaning the buffer. The ob_get_clean() function is the combination of both [ob_get_contents()](https://www.php.net/manual/en/function.ob-get-contents.php) and [ob_end_clean()](https://www.php.net/manual/en/function.ob-end-clean.php).

### PrintF

```php
$number = 9;
$str = "Beijing";
printf("There are %u million bicycles in %s.",$number,$str);

// Output: There are 9 million bicycles in Beijing.
```

The [printf()](https://www.php.net/manual/en/function.printf.php) function outputs a formatted string. The arg1, arg2 parameters will be inserted at percent (%) signs in the main string. This function works "step-by-step". At the first % sign, arg1 is inserted, at the second % sign, arg2 is inserted, etc.

### Global Variables

**Usage in app**: We stored the connection informations and functions in a global variable.  
**Definition**: The [global](https://www.php.net/manual/en/language.variables.scope.php) keyword imports variables from the global scope into the local scope of a function.

```php
$x = 5;
function add($y) {
  global $x;
  return $x + $y;
}

echo "$x + 5 is " . add(5);

// Output: 5 + 5 is 10
```