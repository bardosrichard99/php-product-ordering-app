<?php
header("Content-type: text/html; charset=utf-8");
require_once "connect.php";
global $conn;

?>
<script>
    // Register checkbox function
    function onRegister() {
        // Get the checkbox
        var checkBox = document.getElementById("registered");
        // Get the output text
        var pwLabel = document.getElementById("pw-label");

        // If the checkbox is checked, display the output text
        if (checkBox.checked == true) {
            pwLabel.style.display = "none";
        } else {
            pwLabel.style.display = "inline";
        }
    }
</script>


<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Order</title>
</head>

<body>
    <h1>Order</h1>
    <h4 id="error" style="color: red"></h4>
    <h4 id="success" style="color: green"></h4>
    <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
        <label for="registered">Already registered</label>
        <input type="checkbox" name="registered" id="registered" onclick="onRegister()" value="Registered">
        <br><br>
        <label for="username">User:</label>
        <input type="text" name="username" id="username">
        <br><br>
        <label id="pw-label">Password:
            <input type="password" name="password" id="password">
        </label>
        <br><br>
        <label for="product">Product:</label>
        <select name="product" id="product">
            <option value="apple">Apple</option>
            <option value="banana">Banana</option>
            <option value="orange">Orange</option>
        </select>
        <br><br>
        <label for="amount">Amount:</label>
        <input type="number" name="amount" id="amount" min="1" max="50" value="1">
        <br><br>
        <button type="submit" name="submit">Send order</button>
    </form>
</body>

</html>

<?php

// Order sending
if (isset($_POST["submit"])) {
    // Get datas from form submit
    $username = $conn->real_escape_string($_POST["username"]);
    $password = $conn->real_escape_string($_POST["password"]);
    $product = $conn->real_escape_string($_POST["product"]);
    $amount = $conn->real_escape_string($_POST["amount"]);

    // Check if they empty
    if (empty($username) || empty($product) || empty($amount)) {
        Message('error', "Please fill in all fields!");
    } else {
        $sql = "SELECT Count(*) FROM users WHERE username = '{$username}'";
        $resultCount = $conn->query($sql);

        // Check if the database request went well
        if ($resultCount->num_rows > 0) {
            $row_result = $resultCount->fetch_assoc();
            // Check if the username exists
            if ($row_result['Count(*)'] <= 0) {
                if (isset($_POST['registered'])) {
                    // IF username not exists BUT registered checked
                    Message('error', "Username not found, please provide a password");
                } else if (empty($password)) {
                    // IF username not exists, registered NOT checked BUT no password
                    Message('error', "Please provide a password");
                } else {
                    // IF username not exists, registered NOT checked, have password
                    // Register
                    $password = sha1($password);
                    Register($conn, $username, $password);
                    // Get the new user's id + Order
                    Order($conn, $username, $product, $amount);
                }
            } else {
                if (isset($_POST['registered'])) {
                    // IF username exists, registered checked
                    // Get user id + Order
                    Order($conn, $username, $product, $amount);
                } else {
                    // IF username exists, registered NOT checked
                    Message('error', "Already existing Username, please check the checkbox");
                }
            }
        } else {
            Message('error', "Unsuccesful database action");
        }

    }
}

function Register($conn, $username, $password)
{
    $sql = "INSERT INTO users (username, password, role) VALUES ('{$username}', '{$password}', 'user')";
    if ($conn->query($sql) === TRUE) {
        Message('success', "Client registered successfully");
    } else {
        Message('error', "Error: {$sql} - {$conn->error}");
    }
}

function Order($conn, $username, $product, $amount)
{
    $sql = "SELECT * FROM users WHERE username = '{$username}'";
    $resultUser = $conn->query($sql);
    $user_datas = $resultUser->fetch_assoc();
    // Add order
    $sql = "INSERT INTO orders (product_name, amount, status, userid) VALUES ('{$product}', '{$amount}', 'open', '{$user_datas['userid']}')";
    if ($conn->query($sql) === TRUE) {
        Message('success', "Client registered successfully & Order sent");
    } else {
        Message('success', "");
        Message('error', "Error: {$sql} - {$conn->error}");
    }
}

function Message($isError, $message)
{
    if ($isError === 'error') {
        ?>
        <script>
            document.querySelector("#error").innerText = "<?php echo $message ?>";
        </script>
        <?php
    } else if ($isError === 'success') {
        ?>
            <script>
                document.querySelector("#success").innerText = "<?php echo $message ?>";
            </script>
        <?php
    }
}

?>