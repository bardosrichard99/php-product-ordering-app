<?php

$servername = "localhost";
$username = "admin";
$password = "admin";
$db_name = "phporderapp";

global $conn;

// Connect
$conn = new mysqli($servername, $username, $password, $db_name);

// Check connection
if ($conn->connect_error) {
    die("Connection error: " . $conn->connect_error);
}

?>