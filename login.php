<?php
session_start();
header("Content-type: text/html; charset=utf-8");
require_once "connect.php";
global $conn;
if (isset($_SESSION['loggedin']))
    header('Location: dashboard.php');
?>

<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>

<body>
    <h1>Login</h1>
    <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
        <h4 id="error" style="color: red"></h4>
        <label for="username">User:</label>
        <input type="text" name="username" id="username">
        <br><br>
        <label for="password">Password:</label>
        <input type="password" name="password" id="password">
        <br><br>
        <button type="submit" name="submit">Login</button>
    </form>
</body>

</html>

<?php
// Login
if (isset($_POST["submit"])) {
    // Get datas from form submit
    $username = $conn->real_escape_string($_POST["username"]);
    $password = $conn->real_escape_string($_POST["password"]);

    // Check if the form fields are empty
    if (empty($username) || empty($password)) {
        ?>
        <script>
            document.querySelector('#error').innerText = "Please fill in both fields!";
        </script>
        <?php
    } else {
        // Check if the login details are correct
        $password = sha1($password); // password hashing
        $sql = "SELECT Count(*) FROM users WHERE username = '{$username}' AND password = '{$password}'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $row_result = $result->fetch_assoc();
            if ($row_result['Count(*)'] > 0) {
                $_SESSION["loggedin"] = true;
                $_SESSION["username"] = $username;
                echo "Sikeres bejelentkezés";
                header('Location: dashboard.php');
                exit();
            } else {
                ?>
                <script>
                    document.querySelector("#error").innerText = "Wrong Username / Password!";
                </script>
                <?php
            }

        } else {
            ?>
            <script>
                document.querySelector("#error").innerText = "Unsuccessful DataBase action";
            </script>
            <?php
        }
    }
}

?>