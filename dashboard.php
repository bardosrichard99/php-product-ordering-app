<?php
session_start();
header("Content-type: text/html; charset=utf-8");
// If not logged in -> redirect to login page
if (empty($_SESSION['loggedin']))
    header('Location: login.php');
require_once "connect.php";
global $conn;
$errormsg;

// GET the logged in user's info
$sql = "SELECT * FROM users WHERE username = '{$_SESSION['username']}'";
$result = $conn->query($sql);
$currentUserDatas = $result->fetch_assoc();

// Role change
if (isset($_POST['rolechange'])) {
    $pickedUsername = $conn->real_escape_string($_POST["users"]);

    if (empty($pickedUsername)) {
        // IF nothing get chosen from dropdown
        $errormsg = array("error", "Please choose a username");
    } else if ($pickedUsername == 'admin') {
        // IF admin get chosen from dropdown
        $errormsg = array("error", "Can not change the admin's role");
    } else if ($pickedUsername == $currentUserDatas["username"]) {
        // IF current user get chosen from dropdown
        $errormsg = array("error", "Can not change your own role");
    } else {
        // GET the chosen user's infos
        $sql = "SELECT * FROM users WHERE username = '{$pickedUsername}'";
        $resultUser = $conn->query($sql);
        $user_datas = $resultUser->fetch_assoc();

        $newRole;
        // Check current role and change it
        $user_datas['role'] == 'admin' ? $newRole = 'user' : $newRole = 'admin';
        // UPDATE chosen user's role
        $sql2 = "UPDATE users SET role='{$newRole}' WHERE username='{$pickedUsername}'";
        if ($conn->query($sql2) === TRUE) {
            $errormsg = array("success", "Client '{$pickedUsername}' role successfully changed");
        } else {
            $errormsg = array("error", "Hiba: {$sql2} - {$conn->error}");
        }
    }
}

// Order status change
if (isset($_POST['orderstatuschange'])) {
    $pickedOrderid = $conn->real_escape_string($_POST["orders"]);

    if (empty($pickedOrderid)) {
        // IF nothing get chosen from dropdown
        $errormsg = array("error", "Please choose an order id");
    } else {
        // GET the chosen order's infos
        $sql = "SELECT * FROM orders WHERE orderid = '{$pickedOrderid}'";
        $resultOrder = $conn->query($sql);
        $order_datas = $resultOrder->fetch_assoc();

        $newStatus;
        // Check current status and change it
        $order_datas['status'] == 'open' ? $newStatus = 'closed' : $newStatus = 'open';
        // UPDATE chosen order's status
        $sql2 = "UPDATE orders SET status='{$newStatus}' WHERE orderid='{$pickedOrderid}'";
        if ($conn->query($sql2) === TRUE) {
            $errormsg = array("success", "Order number '{$pickedOrderid}' status successfully changed");
        } else {
            $errormsg = array("error", "Hiba: {$sql2} - {$conn->error}");
        }
    }
}
?>

<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <style>
        table,
        th,
        td {
            border: 1px solid;
            padding: 0.4rem;
        }

        table {
            border-collapse: collapse;
        }
    </style>
</head>

<body id="content">

    <h1>Welcome,
        <?= $_SESSION['username'] ?>!
    </h1>

    <section id="logout-form">
        <form id="logout-action" action="logout.php" method="post">
            <a href="javascript:{}" onclick="document.getElementById('logout-action').submit();">Log out</a>
        </form>
    </section>

    <section id="section-users">
        <h3>User datas</h3>
        <table>
            <thead>
                <tr>
                    <td>ID</td>
                    <td>User</td>
                    <td>Password</td>
                    <td>Role</td>
                </tr>
            </thead>
            <tbody>
                <?php
                // IF current user has admin role -> GET all user data
                // IF not -> GET only the current user's data
                if ($currentUserDatas['role'] == 'admin') {
                    $sql = 'SELECT * FROM users';
                } else {
                    $sql = 'SELECT * FROM users WHERE userid = ' . $currentUserDatas['userid'];
                }
                $result = $conn->query($sql);
                // Listing the rows in table
                while ($row = $result->fetch_assoc()) {
                    printf('
                        <tr>
                            <td>%s</td>
                            <td>%s</td>
                            <td>%s</td>
                            <td>%s</td>
                        </tr>
                        ', $row['userid'], $row['username'], $row['password'], $row['role']);
                }
                ;
                ?>
            </tbody>
        </table>
    </section>

    <section id="section-orders">
        <h3>Order list</h3>
        <table>
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Product</td>
                    <td>Amount</td>
                    <td>Status</td>
                    <td>User ID</td>
                </tr>
            </thead>
            <tbody>
                <?php
                // IF current user has admin role -> GET all order data
                // IF not -> GET only the oders connected to current user
                if ($currentUserDatas['role'] == 'admin') {
                    $sql = 'SELECT * FROM orders';
                } else {
                    $sql = 'SELECT * FROM orders WHERE userid = ' . $currentUserDatas['userid'];
                }
                $result = $conn->query($sql);
                // Listing the rows in table
                while ($row = $result->fetch_assoc()) {
                    printf('
        <tr>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
        </tr>
        ', $row['orderid'], $row['product_name'], $row['amount'], $row['status'], $row['userid']);
                }
                ;
                ?>
            </tbody>
        </table>
    </section>

    <?php ob_start(); //Making an output buffer (HTML below this point only seeable to user with 'admin' role)?>
    <section>
        <h4 id="error" style="color: red;"></h4>
        <h4 id="success" style="color: green;"></h4>
    </section>
    <section>
        <h3>User role change (by ursername)</h3>
        <form id="rolechange-action" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
            <select name="users" id="users">
                <?php
                // Filling dropdown with usernames
                $sql = 'SELECT * FROM users';
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc())
                    printf('<option value="%s">%s</option>', $row['username'], $row['username']);
                ?>
            </select>
            <input type="submit" name="rolechange" value="Change Role">
        </form>
    </section>

    <section>
        <h3>Order status change (by ID)</h3>
        <form id="orderstatuschange-action" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
            <select name="orders" id="orders">
                <?php
                // Filling dropdown with order IDs
                $sql = 'SELECT * FROM orders';
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc())
                    printf('<option value="%s">%s</option>', $row['orderid'], $row['orderid']);
                ?>
            </select>
            <input type="submit" name="orderstatuschange" value="Change Order Status">
        </form>
        <br>
    </section>
</body>

</html>

<?php
$ob_result = ob_get_clean();

// IF current user has admin role, changing sections become seeable
if ($currentUserDatas['role'] === "admin") {
    echo $ob_result;
}

// Showing error messages
if (isset($errormsg) and $errormsg[0] === 'error') {
    ?>
    <script>
        document.querySelector("#error").innerText = "<?php echo $errormsg[1] ?>";
    </script>
    <?php
} else if (isset($errormsg) and $errormsg[0] === 'success') {
    ?>
        <script>
            document.querySelector("#success").innerText = "<?php echo $errormsg[1] ?>";
        </script>
    <?php
}
?>